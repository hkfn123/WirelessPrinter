Pod::Spec.new do |s|

  s.name         = "WirelessPrinter"
  s.version      = "0.0.1"
  s.summary      = "WirelessPrinter Framework"
  s.description  = <<-DESC
  WirelessPrinter Framework
                   DESC
  s.homepage     = "https://gitlab.com/hkfn123/WirelessPrinter"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "jajeo" => "hkfn123@163.com" }
  s.source       = { :git => "https://gitlab.com/hkfn123/WirelessPrinter.git", :tag => "0.0.1" }
  s.source_files  = "WirelessPrinter", "WirelessPrinter/**/*.{h,m}"
  s.vendored_libraries = 'WirelessPrinter/*.framework'

end
