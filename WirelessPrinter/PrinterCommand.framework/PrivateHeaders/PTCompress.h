//
//  PTCompress.h
//  PrinterCommand
//
//  Created by midmirror on 16/3/29.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PTCompress : NSObject

/** TIFF压缩算法 */
bool TIFFDataCompress(unsigned char *inData,long lineLen,long lineCnt,unsigned char *outData,long *outCnt);

/** ZPL2压缩算法 */
bool ZPL2DataCompress(unsigned char *inData,long lineLen,long lineCnt,unsigned char *outData,long *outCnt);

@end
