//
//  PTEncode.h
//  WirelessPrinter
//
//  Created by midmirror on 16/7/27.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PTEncode : NSObject

/** 编码 */
+ (NSData *)encodeDataWithString:(NSString *)string;
/** 解码 */
+ (NSString *)decodeStringWithData:(NSData *)data;

@end
