//
//  PTCommandCPCLSlim.h
//  WirelessPrinter
//
//  Created by midmirror on 16/8/30.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PTCommandCPCLSlim : NSObject

@property(strong,nonatomic,readwrite) NSMutableData * _Nonnull cmdData;

- (void)appendCommand:(NSString * _Nonnull)cmd;

- (void)appendCommandData:(NSData * _Nonnull)data;
- (void)cpclUnderlineON;
- (void)cpclUnderlineOFF;
- (void)cpclUtilitySession;

/** 行模式指令前缀 */
- (void)cpclLineMode;
- (void)cpclReWindOFF;

- (void)cpclLabelWithOffset:(NSInteger)offset
                       hRes:(NSInteger)hRes
                       vRes:(NSInteger)vRes
                     height:(NSInteger)height
                   quantity:(NSInteger)quantity;

- (void)cpclBarcode:(NSString * _Nonnull)type
              width:(NSInteger)width
              ratio:(NSInteger)ratio
             height:(NSInteger)height
                  x:(NSInteger)x
                  y:(NSInteger)y
            barcode:(NSString * _Nonnull)barcode;

- (void)cpclBarcodeVertical:(NSString * _Nonnull)type
                      width:(NSInteger)width
                      ratio:(NSInteger)ratio
                     height:(NSInteger)height
                          x:(NSInteger)x
                          y:(NSInteger)y
                    barcode:(NSString * _Nonnull)barcode;

- (void)cpclBarcodeQRcodeWithXPos:(NSInteger)xPos
                             yPos:(NSInteger)yPos
                            model:(NSInteger)model
                        unitWidth:(NSInteger)unitWidth;

- (void)cpclBarcodeQRcodeVerticalWithXPos:(NSInteger)xPos
                                     yPos:(NSInteger)yPos
                                    model:(NSInteger)model
                                unitWidth:(NSInteger)unitWidth;

- (void)cpclBarcodeQRcodeData:(NSString * _Nonnull)data config:(NSString * _Nonnull)config;

- (void)cpclBarcodeQRcodeEnd;

- (void)cpclBarcodeTextWithFont:(NSInteger)font
                       fontSize:(NSInteger)fontSize
                         offset:(NSInteger)offset;

- (void)cpclBarcodeTextWithTrueTypeFont:(NSInteger)font
                                 xScale:(NSInteger)xScale
                                 yScale:(NSInteger)yScale
                                 offset:(NSInteger)offset;

- (void)cpclBarcodeTextOff;

- (void)cpclBoxWithXPos:(NSInteger)xPos
                   yPos:(NSInteger)yPos
                   xEnd:(NSInteger)xEnd
                   yEnd:(NSInteger)yEnd
              thickness:(NSInteger)thickness;

- (void)cpclCenterWithRange:(NSInteger)range;

- (void)cpclCenter;

- (void)cpclCompressedGraphicsWithImageWidth:(NSInteger)imageWidth
                                 imageHeight:(NSInteger)imageHeight
                                           x:(NSInteger)x
                                           y:(NSInteger)y
                                  bitmapData:(NSData * _Nonnull)bitmapData;

- (void)cpclConcatStartWithXPos:(NSInteger)xPos yPos:(NSInteger)yPos;
- (void)cpclConcatVerticalStartWithXPos:(NSInteger)xPos yPos:(NSInteger)yPos;

/** 字段拼接 */
- (void)cpclConcatTextWithFont:(NSInteger)font
                      fontSize:(NSInteger)fontSize
                        offset:(NSInteger)offset
                          text:(NSString * _Nonnull)text;

- (void)cpclConcatScaleTextWithScaledFont:(NSInteger)scaledFont
                                   xScale:(NSInteger)xScale
                                   yScale:(NSInteger)yScale
                                   offset:(NSInteger)offset
                                     text:(NSString * _Nonnull)text;

- (void)cpclConcatScaleTextVerticalWithScaledFont:(NSInteger)scaledFont
                                           xScale:(NSInteger)xScale
                                           yScale:(NSInteger)yScale
                                           offset:(NSInteger)offset
                                             text:(NSString * _Nonnull)text;

- (void)cpclConcatTextWithFontGroup:(NSInteger)fontGroup
                             offset:(NSInteger)offset
                               text:(NSString * _Nonnull)text;

- (void)cpclConcatEnd;
- (void)cpclPrint;
- (void)cpclInverseLineWithXPos:(NSInteger)xPos
                           yPos:(NSInteger)yPos
                           xEnd:(NSInteger)xEnd
                           yEnd:(NSInteger)yEnd
                      thickness:(NSInteger)thickness;

- (void)cpclLeft:(NSInteger)range;

- (void)cpclLeft;

- (void)cpclLineWithXPos:(NSInteger)xPos
                    yPos:(NSInteger)yPos
                    xEnd:(NSInteger)xEnd
                    yEnd:(NSInteger)yEnd
               thickness:(NSInteger)thickness;

- (void)cpclMoveWithRight:(NSInteger)right up:(NSString * _Nonnull)up;
- (void)cpclMultiLineStartWithLineHeight:(NSInteger)lineHeight;
- (void)cpclMultiLineEnd;
- (void)cpclPageWidth:(NSInteger)pageWidth;
- (void)cpclRight:(NSInteger)right;
- (void)cpclRight;
- (void)cpclRotate:(NSInteger)degrees;
- (void)cpclScaleText:(NSString * _Nonnull)scaledFont
               xScale:(NSInteger)xScale
               yScale:(NSInteger)yScale
                    x:(NSInteger)x
                    y:(NSInteger)y
                 text:(NSString * _Nonnull)text;

- (void)cpclScaleTextVertical:(NSString * _Nonnull)scaledFont
                       xScale:(NSInteger)xScale
                       yScale:(NSInteger)yScale
                            x:(NSInteger)x
                            y:(NSInteger)y
                         text:(NSString * _Nonnull)text;

- (void)cpclScaleToFit:(NSString * _Nonnull)scaleFont
                 width:(NSInteger)width
                height:(NSInteger)height
                     x:(NSInteger)x
                     y:(NSInteger)y
                  text:(NSString * _Nonnull)text;

- (void)cpclSetBold:(NSInteger)boldness;
- (void)cpclSetSpacing:(NSInteger)spacing;
- (void)cpclSetMagWithWidth:(NSInteger)width height:(NSInteger)height;
- (void)cpclTempMove:(NSInteger)right up:(NSInteger)up;

- (void)cpclTextWithRotate:(NSInteger)rotate
                      font:(NSInteger)font
                  fontSize:(NSInteger)fontSize
                         x:(NSInteger)x
                         y:(NSInteger)y
                      text:(NSString * _Nonnull)text;

- (void)cpclTextWithRotate:(NSInteger)rotate
              trueTypeFont:(NSInteger)trueTypeFont
                    xScale:(NSInteger)xScale
                    yScale:(NSInteger)yScale
                         x:(NSInteger)x
                         y:(NSInteger)y
                      text:(NSString * _Nonnull)text;

- (void)cpclTextWithRotate:(NSInteger)rotate
                 fontGroup:(NSInteger)fontGroup
                         x:(NSInteger)x
                         y:(NSInteger)y
                      text:(NSString * _Nonnull)text;

- (void)cpclTextReverseWithFont:(NSInteger)font
                       fontSize:(NSInteger)fontSize
                              x:(NSInteger)x
                              y:(NSInteger)y
                           text:(NSString * _Nonnull)text;

/***************** Line Print Commands *******************/

- (void)cpclLineMargin:(NSInteger)offset;
- (void)cpclSetPositionWithXPos:(NSInteger)xPos yPos:(NSInteger)yPos;
- (void)cpclSetPositionWithXPos:(NSInteger)xPos;
- (void)cpclSetPositionWithYPos:(NSInteger)yPos;
- (void)cpclLineFeed;
- (void)cpclContrast:(NSInteger)value;
- (void)cpclFeed:(NSInteger)amount;
- (void)cpclLabel;
- (void)cpclMulti:(NSInteger)quantity;
- (void)cpclNoPace;
- (void)cpclPace;
- (void)cpclPostFeed:(NSInteger)amount;
- (void)cpclPreFeed:(NSInteger)amount;
- (void)cpclReverse:(NSInteger)amount;
- (void)cpclSetFeed:(NSInteger)length skip:(NSInteger)skip;
- (void)cpclSpeed:(NSInteger)value;
- (void)cpclForm;
- (void)cpclTone:(NSInteger)value;
- (void)cpclTurn:(NSInteger)degrees;
- (void)cpclFormFeed;

/****************** Utility and Diagnostic Commands ****************/
- (void)cpclAbort;
- (void)cpclOnFeed_Feed;
- (void)cpclOnFeed_Reprint;
- (void)cpclOnFeed_Ignore;
- (void)cpclReRun;
- (void)cpclWait:(NSInteger)duration;
- (void)cpclSetLabelPositionWithXPos:(NSInteger)xPos yPos:(NSInteger)yPos;
- (void)cpclSetLabelPositionWithXPos:(NSInteger)xPos;
- (void)cpclSetLabelPositionWithYPos:(NSInteger)yPos;

@end
