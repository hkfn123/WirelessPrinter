//
//  UIImage+Extensions.h
//  mPrinter
//
//  Created by Andy Muldowney on 7/5/13.
//  Copyright (c) 2013 mPrinter, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Dithering)

- (UIImage *)imageWithDithering;
- (NSData *)ditheringWithFloydSteinberg;
- (void)imageDump;

@end