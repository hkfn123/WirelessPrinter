//
//  PTBitmap.H
//
//  Created by midmirror on 15/12/22.
//  Copyright © 2015年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CommandDefine.h"

/**
 *  Only MPT-8 support compression algorithm:ZPL2/TIFF. Other printer choose Null please.
 *  MPT-8 支持的图片压缩算法:ZPL2压缩算法，TIFF 压缩算法,其他机型选择 NUll
 */
typedef NS_ENUM(NSInteger,PTBitmapCompressMode) {
    
    PTBitmapCompressModeNone = 0,
    PTBitmapCompressModeZPL2 = 16,
    PTBitmapCompressModeTIFF = 32
};

typedef NS_ENUM(NSInteger, PTBitmapMode) {
    
    PTBitmapModeBinary = 0,
    PTBitmapModeDithering = 1,
};

/**
 *  位图处理：生成黑白二值图片或者抖动后的视觉灰度图片
 */
@interface PTBitmap : NSObject{
    UIImage *m_image;
    NSData *starImageData;
    bool ditheringSupported;
}

singletonH(PTBitmap)

+ (UIImage *)scaleSourceImage:(UIImage *)image maxWidth:(CGFloat)maxWidth;

/**
 * Print binary bitmap (black white bitmap)
 * 打印黑白二值图像
 * Print Dither Bitmap
 * 打印经过抖动的视觉灰度图
 *
 *  @param image  image
 *
 *  @return a generated data to print
 */
+ (NSData *)getImageData:(UIImage *)image mode:(PTBitmapMode)mode compress:(PTBitmapCompressMode)compress;

@end
