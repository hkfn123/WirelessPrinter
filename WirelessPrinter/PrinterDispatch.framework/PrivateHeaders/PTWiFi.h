//
//  PTNetwork.h
//  PrinterDispatch
//
//  Created by midmirror on 16/2/23.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTAsyncSocket.h"
#import "PTPrinter.h"
#import "PTDispatcher.h"
#import "CommandDefine.h"

/**
 *  功能：用于和打印机的 WiFi 建立连接实现通讯。使用了 AsyncSocket 第三方框架。
 */
@interface PTWiFi : NSObject<PTAsyncSocketDelegate>

singletonH(PTWiFi)

@property(strong,nonatomic,readwrite) PTAsyncSocket *asyncSocket;
@property(weak,nonatomic,readwrite) id<PTPrinterDelegate> delegate;

- (void)connectWiFi:(NSString *)ip port:(NSString *)port;
- (void)disConnectPrinter;
- (void)initNetWork;
- (void)initParameter;

- (void)sendData:(NSData *)data;
- (void)sendDataQueue:(NSArray *)dataQueue;

@end
