//
//  PTDispatchLog.h
//  PrinterCommand
//
//  Created by midmirror on 16/3/24.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DispatcherDefine.h"

@interface PTDispatchLog : NSObject

singletonH(PTDispatchLog)

/**
 *  用户保存和打印机的会话消息
 */
@property(strong,nonatomic,readwrite) NSMutableArray *dispatchLogArray;

/**
 *  打印机的自我介绍
 *  printer self-introduction
 *
 *  @param name        打印机名字
 *  @param commandType 指令类型
 *  @param printerType 打印机类型
 *  @param linkType    连接方式
 */
- (void)selfIntroductionWithName:(NSString *)name
                     commandType:(NSString *)commandType
                     printerType:(NSString *)printerType
                        linkType:(NSString *)linkType;

- (void)addUserDispatchLog:(NSString *)log;

- (void)addUserDispatchLogImage:(UIImage *)image;

- (void)addPrinterDispatchLog:(NSString *)log;

@end
