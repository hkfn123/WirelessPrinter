//
//  PTPrinter.h
//  WirelessPrinter
//
//  Created by midmirror on 16/9/9.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

/** 当前打印机配备哪些模块 */
typedef NS_ENUM(NSInteger, PTPrinterModule) {
    
    PTPrinterModuleUnknown    = 0,
    PTPrinterModuleBLE        = 1,
    PTPrinterModuleWiFi       = 2,
    PTPrinterModuleBoth       = 3,
};

typedef NS_ENUM(NSInteger, PTCommandMode) {
    
    PTCommandModeUnselected = -1,
    PTCommandModeESC        = 0,
    PTCommandModeTSPL       = 1,
    PTCommandModeCPCL       = 2,
    PTCommandModeDPL        = 3,
    PTCommandModeEPL        = 4,
    PTCommandModeZPL        = 5,
    PTCommandModeSTAR       = 6,
};

@interface PTPrinter : NSObject

@property(strong,nonatomic,readwrite) NSString *name;       // Printer's name
@property(strong,nonatomic,readwrite) NSString *MAC;        // Printer's BLE or WiFi MAC address
@property(strong,nonatomic,readwrite) NSString *MACKey;
@property(strong,nonatomic,readwrite) NSString *lastTime;
@property(assign,nonatomic,readwrite) PTPrinterModule module;
@property(assign,nonatomic,readwrite) PTCommandMode commandMode;

// BLE
@property(strong,nonatomic,readwrite) NSString *UUID;       // Printer's Unique Identifier
@property(strong,nonatomic,readwrite) NSNumber *RSSI;       // Printer's Siginal strength
@property(strong,nonatomic,readwrite) NSNumber *strength;
@property(strong,nonatomic,readwrite) NSNumber *distance;   // Printer's distance calculate by RSSI
@property(assign,nonatomic,readwrite) BOOL listening;       // when value is YES, SDK will get printer's message
@property(strong,nonatomic,readwrite) CBPeripheral *peripheral;

// WiFi
@property(strong,nonatomic,readwrite) NSString *IP;
@property(strong,nonatomic,readwrite) NSString *port;

/** 获取打印机名字 */
+ (NSString *)nameWithPeripheral:(CBPeripheral *)peripheral advertisement:(NSDictionary *)advertisement;

/** 获取 MAC 地址 */
+ (NSString *)MACWithAdvertisement:(NSDictionary *)advertisement;
+ (NSString *)MACKeyWithMAC:(NSString *)MAC;

/** 计算距离 */
+ (NSNumber *)distanceWithRSSI:(NSNumber *)RSSI;

/** 判断信号强度 */
+ (NSNumber *)strengthWithRSSI:(NSNumber *)RSSI;

@end
