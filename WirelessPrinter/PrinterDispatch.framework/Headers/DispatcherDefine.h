//
//  PublicDefine.h
//  WirelessPrinter
//
//  Created by midmirror on 16/4/27.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#ifndef PublicDefine_h
#define PublicDefine_h

/*
 用于生成一个单例
 */

// @interface
#define singletonH(className) \
+ (className *)share;

// @implement
#define singletonM(className) \
static className *instance; \
+ (className *)share \
{ \
static dispatch_once_t once; \
dispatch_once(&once, ^{ \
instance = [[self alloc] init]; \
}); \
return instance; \
}


#define kMSG_CONTENT_TEXT     @"MSG_CONTENT_TEXT"
#define kMSG_CONTENT_IMAGE    @"MSG_CONTENT_IMAGE"
#define kMSG_NAME             @"MSG_NAME"
#define kMSG_NAME_PRINTER     @"MSG_NAME_PRINTER"
#define kMSG_NAME_USER        @"MSG_NAME_USER"
#define kMSG_TIMESTAMP        @"MSG_TIMESTAMP"

#endif /* PublicDefine_h */
