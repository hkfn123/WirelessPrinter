//
//  PTSettings.h
//  PrinterCommand
//
//  Created by midmirror on 16/4/8.
//  Copyright © 2016年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kThemeColor @"kThemeColor"

@interface PTSettings : NSObject

/** read setting from userDefaults 读取用户设置 */
+ (id)getUserDefaultObjectByKey:(NSString *)key;

/** save setting to userDefaults 保存用户设置 */
+ (void)saveUserDefaultObject:(id)object key:(NSString *)key;

@end
