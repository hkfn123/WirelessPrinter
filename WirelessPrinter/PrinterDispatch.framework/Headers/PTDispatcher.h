//
//  PTPrinter.h
//
//  Created by midmirror on 15/12/25.
//  Copyright © 2015年 midmirror. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "DispatcherDefine.h"
#import "PTPrinter.h"

/** 当前通讯方式 */
typedef NS_ENUM(NSInteger, PTDispatchMode) {
    
    PTDispatchModeUnconnect  = 0,
    PTDispatchModeBLE        = 1,
    PTDispatchModeWiFi       = 2,
};

@class PTPrinter;

/**
 *  Protocol 功能：一些蓝牙和 WiFi 建立连接时的代理方法。注册相应的通知也可以实现一样的功能
 */
@protocol PTPrinterDelegate <NSObject>

@optional

- (void)didFindBLEPrinter:(PTPrinter *)printer;
- (void)didFindBLEPrinters:(NSArray *)printers;
- (void)didFindWiFiPrinters:(NSDictionary *)printers;

- (void)didConnectPrinter:(PTPrinter *)printer;

- (void)didDisconnectBluetooth;

@end

/** 发现新蓝牙 */
typedef void(^ScanPrinterBlock)(PTPrinter *printer);
typedef void(^ScanPrintersBlock)(NSDictionary *printers);

/** 连接成功、失败 */
typedef void(^ConnectSuccessBlock)();
typedef void(^ConnectFailureBlock)();

/** 可以发送、发送成功、发送失败、接收到返回值 */
typedef void(^BLEReadyBlock)();
typedef void(^SendSuccessBlock)();
typedef void(^ReceiveBlock)(NSData *receiveData);

/** 断开 */
typedef void(^DisconnectBlock)();

//监听通知 获取蓝牙的各种状态（更新 RSSI、WiFI 模式，断线模式、更新返回值）
static NSString *const PTPrinterDidUpdateRSSINotification    = @"PTPrinterDidUpdateRSSINotification";
static NSString *const PTPrinterDidTurnBLENotification       = @"PTPrinterDidTurnBLENotification";
static NSString *const PTPrinterDidTurnWiFiNotification      = @"PTPrinterDidTurnWiFiNotification";
static NSString *const PTPrinterDidTurnOfflineNotification   = @"PTPrinterDidTurnOfflineNotification";
static NSString *const PTPrinterDidReceiveNotification       = @"PTPrinterDidReceiveNotification";

/**
 *  主要功能：实现 SDK 和打印机的通讯，主要是BLE 蓝牙和 WiFi 两种方式通讯
 */
@interface PTDispatcher : NSObject

singletonH(PTDispatcher)

typedef NS_ENUM(NSInteger, PTPrinterMediaType) {
    
    PTPrinterMediaTypeUnknown   = -1,
    PTPrinterMediaTypeText      = 0,
    PTPrinterMediaTypeBarcode   = 1,
    PTPrinterMediaTypeImage     = 2,
    PTPrinterMediaTypeLabel     = 3,
};

@property(strong,nonatomic,readwrite) PTPrinter *printerConnected;
@property(assign,nonatomic,readwrite) PTDispatchMode mode;

@property(copy,nonatomic,readwrite) ReceiveBlock         receiveBlock;                //发送结束，且收到返回值时执行的 Block
@property(copy,nonatomic,readwrite) SendSuccessBlock     sendSuccessBlock;        //发送结束时的 Block
@property(copy,nonatomic,readwrite) ScanPrinterBlock     findBLEPrinterBlock;
@property(copy,nonatomic,readwrite) ScanPrintersBlock    findBLEPrintersBlock;
@property(copy,nonatomic,readwrite) ScanPrinterBlock     findWiFiPrinterBlock;
@property(copy,nonatomic,readwrite) ScanPrintersBlock    findWiFiPrintersBlock;
@property(copy,nonatomic,readwrite) ConnectSuccessBlock  connectSuccessBlock;
@property(copy,nonatomic,readwrite) ConnectFailureBlock  connectFailureBlock;
@property(copy,nonatomic,readwrite) BLEReadyBlock        readyBlock;
@property(copy,nonatomic,readwrite) DisconnectBlock      disconnectBlock;

/** 单纯发送数据给打印机 */
- (void)sendData:(NSData *)data;
/** 发送数据，并等待打印机返回值 */
- (void)sendData:(NSData *)data receive:(ReceiveBlock)receiveBlock;
/** 发送数据，并等待成功发送结束，或者发送失败原因 */
- (void)sendData:(NSData *)data success:(SendSuccessBlock)successBlock disconnect:(DisconnectBlock)disconnectBlock;
- (void)sendData:(NSData *)data success:(SendSuccessBlock)successBlock receive:(ReceiveBlock)receiveBlock disconnect:(DisconnectBlock)disconnectBlock;
- (void)sendDataQueue:(NSArray *)dataQueue;

#pragma mark PTBluetooth

/** 扫描蓝牙 */
- (void)scanBLEPrinterWithDelegate:(id)delegate;
- (void)scanBLEPrinter:(ScanPrinterBlock)BLEPrinterBlock;
- (void)scanBLEPrinters:(ScanPrintersBlock)BLEPrintersBlock;

/** 停止扫描蓝牙 */
- (void)stopScanBluetooth;

/** 连接打印机 */
- (void)connectPrinter:(PTPrinter *)printer;
- (void)connectPrinter:(PTPrinter *)printer
               success:(ConnectSuccessBlock)successBlock
               failure:(ConnectFailureBlock)failureBlock
                 ready:(BLEReadyBlock)readyBlock;

/** 断开连接蓝牙 */
- (void)disConnectBluetooth;
- (void)disConnectBluetooth:(DisconnectBlock)disconnectBlock;
/** 取消当前连接（Wifi 或者 BLE）*/
- (void)disConnectPrinter;

- (void)scanWiFiPrinterWithDelegate:(id)delegate;

#pragma mark PTNetWork

@end
